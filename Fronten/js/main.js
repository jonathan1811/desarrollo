(function(){
  
  let sticky = false

  $("#sticky-navigation").removeClass("hidden")
  $("#sticky-navigation").slideUp(0)

  $(window).scroll(()=>{
  	const inBottom = isInBottom()

  	if(inBottom && !sticky){
      //Mostrar la navegacion
      console.log("Cambiar la navegacion")
      sticky = true
      stickNavigation()
  	}
   if(!inBottom && sticky){
   	  //Ocultar la navegacion
        sticky = false
       console.log("Regresar la navegacion")
       unStickNavigation()
  	}
  })

  function stickNavigation(){
  	$("#description").addClass("fixed").removeClass("absolute")
  	$("#navegacion").slideUp()
  	$("#sticky-navigation").slideDown("fast")
    $("#navigation").removeClass("fixed")

  }

  function unStickNavigation(){
  	$("#description").removeClass("fixed").addClass("absolute")
  	$("#navegacion").slideDown("fast")
  	$("#sticky-navigation").slideUp("fast")
    $("#navigation").addClass("fixed")


  }

function isInBottom(){

  const $description = $("#description")
  const descriptionHeight = $description.height()

  return $(window).scrollTop() > $(window).height() - (descriptionHeight *2)
} 

})()